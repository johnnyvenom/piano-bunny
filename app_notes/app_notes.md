Piano Bunny
===========

## Objective: 

Build an application that teaching kids to learn to play the piano in a game environment.

App build for Becky Reesor. 

**See flow chart v1 for gameplay flow.**

### Requirements: 

- Screen shows users hand(s) and indicates what notes to play by a bouncing bunny (?) over the correct finger
- Connected to MIDI keyboard for user input
- Several phases/levels, total game time around 1 hour. 
- Scoring and evaluation logs
- Beta version in 1-2 months, final version early 2018

### Questions: 

- What platform? Mac/Windows/iOS/Android? Web?
    + Windows primarily
- How will scoring work? 
- What logs/reports need to be generated? 

Practice - 1 note at a time until > 70bpm w no improvement
- not checking for note error. 

Data collection - response time - speed and accuracy

-----------

## Specs: 

### Hardware: 

- Windows laptop, running standalone application built in Max, positioned in front of and controlled by experimenter. 
- 2nd display connected to laptop showing gameplay window in fullscreen, positioned in front of participant at keyboard. 
- Midi enabled keyboard, connected to laptop via USB (if available) or standard MIDI cable to MIDI interface. 

### Software - 

- Standalone application built with Max (Cycling '74)
- Outputs to 2 visual displays: 
    + Main gameplay display for participant sitting in front of keyboard (via connected 2nd screen)
    + Control display for experimenter, with UI controls for the game. 
- Generates a text file for each participant, recording the following data during each gameplay phase, for each turn (note): 
    + Note to play (1 - 5, where the bunny goes)
    + Note played (1 - 5, what note the participant played)
    + Time bunny appears (in milliseconds)
    + Time note is played (in milliseconds)
- Technical specifications for application:
    + Gameplay scene is rendered in openGL scene, with hand and bunnies 

## Gameplay

0. Training phase,
1. C1: Learning phase - unexposed structure,
2. C1: Test phase,
3. **1 MINUTE BREAK**,
4. C1: Control sequence learning phase - 256 random digits,
5. C1: Control sequence test phase,
6. **5 MINUTE BREAK**,
7. Condition 2: Learning phase - exposed structure,
8. Condition 2: Test phase,
9. **1 MINUTE BREAK**,
10. C2: Control sequence learning phase - 256 random digits,
11. C2: Control sequence test phase, 
12. **Break??**,
13. Application Sequence: Learning phase,
14. Application Sequence: Test phase,
15. Post experiment: Interview Questions,

### Phase 2: 

"...until participant reaches a consistent rate above 70 bpm."


    Steps and parameter settings: 

    --. App launch
        - user display: 
            - background: 
                - load keyboard (loadbang)
                - default BG color
            - hand: load bunny_red_hat (to-hand)
            - bunny: enable 0 (to-bunny)
            - console: "Welcome to Piano Bunny" (to-gameplay-console)
        - controller: 
            - umenu: Welcome (loadbang)
            - console: Begin message (to-console)
            - button: display BEGIN, active 1 (to-button)
        - ACTION: press BEGIN
    00. Session setup
        - user display: 
            - hand: enable 0
            - console: "getting things ready"
        - controller: 
            - console: "create the logfile"
            - button: active 0
        ACTION: name and save file
        - controller console: "saved as..."
    01. Training phase
        - user display: 
            - background: new color
            - hand: load hand and enable
            - bunny: brown bunny zoom to position 6
            - console: Follow the Bunny! 
        - controller: 
            - console: instructions
            - button: START, enable 1
        ACTION: press START
        - user display: 
            - console: ready, 3, 2, 1, go
            - bunny: scale to .15
        GAMEPLAY BEGINS
        - controller console: "gameplay in progress"

    02. C1: Learning phase - unexposed structure
    03. C1: Test phase
    04. **1 MINUTE BREAK**
    05. C1: Control sequence learning phase - 256 random digits
    06. C1: Control sequence test phase
    07. **5 MINUTE BREAK**
    08. Condition 2: Learning phase - exposed structure
    09. Condition 2: Test phase
    10. **1 MINUTE BREAK**
    11. C2: Control sequence learning phase - 256 random digits
    12. C2: Control sequence test phase
    13. **Break??**
    14. Application Sequence: Learning phase
    15. Application Sequence: Test phase
    16. Post experiment: Interview Questions


Background colors: 


#####  Color Palette by Paletton.com
#####  Palette URL: http://paletton.com/#uid=75e0u0kllllaFw0g0qFqFg0w0aF


*** Primary color:

   shade 0 = #8D2F5D = rgb(141, 47, 93) = rgba(141, 47, 93,1) = rgb0(0.553,0.184,0.365)
   shade 1 = #D38DAF = rgb(211,141,175) = rgba(211,141,175,1) = rgb0(0.827,0.553,0.686)
   shade 2 = #B05883 = rgb(176, 88,131) = rgba(176, 88,131,1) = rgb0(0.69,0.345,0.514)
   shade 3 = #6A123D = rgb(106, 18, 61) = rgba(106, 18, 61,1) = rgb0(0.416,0.071,0.239)
   shade 4 = #460023 = rgb( 70,  0, 35) = rgba( 70,  0, 35,1) = rgb0(0.275,0,0.137)

*** Secondary color (1):

   shade 0 = #AA4339 = rgb(170, 67, 57) = rgba(170, 67, 57,1) = rgb0(0.667,0.263,0.224)
   shade 1 = #FFB2AA = rgb(255,178,170) = rgba(255,178,170,1) = rgb0(1,0.698,0.667)
   shade 2 = #D4746A = rgb(212,116,106) = rgba(212,116,106,1) = rgb0(0.831,0.455,0.416)
   shade 3 = #801F15 = rgb(128, 31, 21) = rgba(128, 31, 21,1) = rgb0(0.502,0.122,0.082)
   shade 4 = #550800 = rgb( 85,  8,  0) = rgba( 85,  8,  0,1) = rgb0(0.333,0.031,0)

*** Secondary color (2):

   shade 0 = #2B803E = rgb( 43,128, 62) = rgba( 43,128, 62,1) = rgb0(0.169,0.502,0.243)
   shade 1 = #80C18F = rgb(128,193,143) = rgba(128,193,143,1) = rgb0(0.502,0.757,0.561)
   shade 2 = #50A162 = rgb( 80,161, 98) = rgba( 80,161, 98,1) = rgb0(0.314,0.631,0.384)
   shade 3 = #106022 = rgb( 16, 96, 34) = rgba( 16, 96, 34,1) = rgb0(0.063,0.376,0.133)
   shade 4 = #00400E = rgb(  0, 64, 14) = rgba(  0, 64, 14,1) = rgb0(0,0.251,0.055)

*** Complement color:

   shade 0 = #759D34 = rgb(117,157, 52) = rgba(117,157, 52,1) = rgb0(0.459,0.616,0.204)
   shade 1 = #CEEC9D = rgb(206,236,157) = rgba(206,236,157,1) = rgb0(0.808,0.925,0.616)
   shade 2 = #9FC462 = rgb(159,196, 98) = rgba(159,196, 98,1) = rgb0(0.624,0.769,0.384)
   shade 3 = #507614 = rgb( 80,118, 20) = rgba( 80,118, 20,1) = rgb0(0.314,0.463,0.078)
   shade 4 = #314F00 = rgb( 49, 79,  0) = rgba( 49, 79,  0,1) = rgb0(0.192,0.31,0)


#####  Generated by Paletton.com (c) 2002-2014

----

Questions for becky: 

1. Learning phases are really long. Is this intended? YES FOR NOW…
2. What color should bunny be for following phases:
    - C1 control sequence - learning (currently mono)
    - C2 testing (currently brown) - CHANGE TO GRAY
    - C2 control sequence - learning (currently pink) - CHANGE TO GRAY
    - C2 control sequence - test (currently pink) - GRAY
3. Audio is ON for C1 and C2 test sequences (it is off for learning). Is this correct? YES, FOR NOW…

Notes about the application: 
- There are 2 displays: Controller display (yours) and user display (the participant's). 
- The user display is a second floating window. Toggle on/off with the ` key or button in settings. Move it to a second display at the keyboard, then toggle fullscreen on with 'esc' key or button in settings. 
- A MIDI keyboard should be used for note entry, though the 1 - 5 keys on the computer work too. Connect the MIDI keyboard to your computer and the game will automatically detect user input. 
- Sound is generated from your computer's General MIDI player. You can connect your laptop to external speakers if desired. For levels without sound, the application automatically mutes output. You can also mute/unmute audio in the settings. 

Playing the game: 
- Press begin and the session will move through all 'phases' of the game sequentially. The current phase is displayed in the title bar.
- You can end any phase of the game you are on with the 'END SECTION AND MOVE ON' button. This will save all data from that phase and move to the end as if you had completed that phase. 
    + 'Learning' phases have no set end point, instead you should press the 'DONE' button when the user has sufficiently completed the phase.
- You can move to any section of the game you like by clicking on the title bar to reveal a dropdown menu. 
- You can quit the session at any time during gameplay with the 'END SESSION' button. This will give an option to quit the game entirely or return to the beginning to start a new session. 

Session Data: 
- Data is written to a single .csv file. The first step of the game is to name the session, create a new data file and select a destination folder to save it to. 
- Data is saved to the file at the end of each phase.  
- If a phase is ended prematurely with the 'END SECTION AND MOVE ON' button, data up to that point is saved. 
- The file contains the following data for each event: 
    + event_number: count of how many notes have elapsed in current phase
    + finger_number: number of note displayed by bunny (1 - 5)
    + note_indicated: MIDI note number of note displayed by bunny (0 - 127)
    + note_played: MIDI note number of note played by user (0 - 127)
    + time_shown: timestamp of when note was displayed (milliseconds)
    + time_played: timestamp of when note was played (milliseconds)
    + elapsed_time: time between note displayed and played (milliseconds)
- During Test Phases, some fields are not used and a 0 is displayed instead. 

——

Revisions - 16-Jan 2018

- [x] Display hand at start of every phase
- [x] fix filepaths to work with Max and standalone versions, Mac and Win
- [x] C2 line 1 fix bunny colors
- [x] set anim time to 0 by default
- [x] increase training phase to 10 note average

Questions for becky: 

1. [x] Learning phases are really long. Is this intended? YES FOR NOW…
2. [x] What color should bunny be for following phases:
    - C1 control sequence - learning (currently mono)
    - C2 testing (currently brown) - CHANGE TO GRAY
    - C2 control sequence - learning (currently pink) - CHANGE TO GRAY
    - C2 control sequence - test (currently pink) - GRAY
3. [x] Audio is ON for C1 and C2 test sequences (it is off for learning). Is this correct? YES, FOR NOW…

----------

