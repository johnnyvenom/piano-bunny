Piano Bunny Application
=======================

Latest release: v1.0 - 8-May-2018
written by John Sullivan | IDMIL, CIRMMT
john.sullivan2@mail.mcgill.ca

## Notes about the application:

- There are 2 displays: Controller display (yours) and user display (the participant's). 
- The user display is a second floating window. Toggle on/off with the ` key or button in settings. Move it to a second display at the keyboard, then toggle fullscreen on with 'esc' key or button in settings. 
- A MIDI keyboard should be used for note entry, though the 1 - 5 keys on the computer work too. Connect the MIDI keyboard to your computer and select it from the 'MIDI keyboard' dropdown menu. 
- Sound should be generated from your computer's General MIDI player - select from the 'MIDI device for playback' menu. You can also route MIDI to any other MIDI playback device/software you like. You can connect your laptop to external speakers if desired. For levels without sound, the application automatically mutes output. You can also mute/unmute audio in the settings. 

## Playing the game: 

- Press begin and the session will move through all 'phases' of the game sequentially. The current phase is displayed in the title bar.
- You can end any phase of the game you are on with the 'END SECTION AND MOVE ON' button. This will save all data from that phase and move to the end as if you had completed that phase. 
    + 'Learning' phases have no set end point, instead you should press the 'DONE' button when the user has sufficiently completed the phase.
- You can move to any section of the game you like by clicking on the title bar to reveal a dropdown menu. 
- You can quit the session at any time during gameplay with the 'END SESSION' button. This will give an option to quit the game entirely or return to the beginning to start a new session. 

## Session Data: 

- Data is written to a single .csv file. The first step of the game is to name the session, create a new data file and select a destination folder to save it to. 
- Data is saved to the file at the end of each phase.  
- If a phase is ended prematurely with the 'END SECTION AND MOVE ON' button, data up to that point is saved. 
- The file contains the following data for each event: 
    + event_number: count of how many notes have elapsed in current phase
    + finger_number: number of note displayed by bunny (1 - 5)
    + note_indicated: MIDI note number of note displayed by bunny (0 - 127)
    + note_played: MIDI note number of note played by user (0 - 127)
    + time_shown (ms): timestamp of when note was displayed (milliseconds)
    + time_played (ms): timestamp of when note was played (milliseconds)
    + elapsed_time (ms): time between note displayed and played (milliseconds)
- During Test Phases, some fields are not used and a 0 is displayed instead. 

----------

    ## Changelog: 

    v1.0 - 8-May - alpha release
        - (28-Feb) created button on UI to open audio preferences. 
        - (28-Feb) game window no longer disappears if mouse clicks outside of Piano Bunny app. 
        - (28-Feb) default bunny animation time is set to 0. 
        - TRAINING PHASE: 
            - (1-May) training phase is now a control sequence with 128 random digits, no repeated patterns and no penalty for error. 
            - (7-May) training phase shortened to 64 digits.
        - CONDITION 1: 
            - (1-May) Cut each stimulus for condition 1 learning phase in half. Instead alternating 256 digits and 30sec break, alternate 64 digits and 30s break    
            - (1-May) control sequence shortened from 256 to 128 digits.
            - (7-May) condition 1 learning phase reverted back to original. 
            - (7-May) control sequence removed. 
        - CONDITION 2: 
            - (1-May) Cut the first 3 stimuli in half, as done for condition 1. Delete the 4th stimulus. 
            - (1-May) control sequence shortened from 256 to 128 digits.
            - (7-May) condition 2 learning phase reverted back to original. 
            - (7-May) control sequence removed.
        - Application sequence: 
            - (1-May) Divide it in half into two 128 digit sequences. Insert at 30 second break so it is the same format as the other two sequences.
            - (7-May) application sequence learning phase reverted back to original.  

    v0.3b - released 26-Feb 2018
        - corrected note sequence in C1 Learning phase.
        - increased default animation time to 10 ms to help minimize visual artifacts during gameplay.
        - additional internal changes to help with visual artifacts.

    v0.3a - released 25-Feb-2018
        - updated rendering engine to fix bunny flashing at wrong place (unconfirmed). 
        - corrected 1st line color sequence in C2 Learning phase.

    v0.3 - released 21-Feb-2018

        - fixed a bug where new bunny color appears before it moves. now if bunny changes it happens after move if animation time is less than 100ms, and at the top of trajectory (hop) if anim. time is more than 100ms. 
        - set default animation time to 5ms to help with bunny flashing at wrong place.
        - recompiled application at 64bit 
        - added MIDI keyboard select menu (still should default to correct keyboard)
        - added '(ms)' to data log column titles
        - now compiled for Mac and Windows (previously just Windows)

    v0.2 - released 01-Feb-2018

        - training phase ends when user enters 15 correct notes in a row.
        - inserted 30 second break after each line x4 reps in Conditions 1 & 2 learning phases. 
        - removed testing phases for control sequences.
        - removed auditory feedback for all levels except application sequences.
        - changes C1 application note sequences.

    v0.1 - released 19-Jan-2018

        Bugfixes: 
        - C2 line 1 corrected bunny colors
        Changes: 
        - changed C2 test, control seq, and ctrl seq test to gray bunny.
        - bunny moves directly to next note, no animation
        - training phase lengthened: 10 note avg to get BPM, 20 note min trial. 
        - Displays hand at start of every phase in case controller skips around. 
        - fix filepaths to work with both Mac/Win versions of Max and standalone apps.

    v0.0 - released 9-Jan-2018

        - initial beta release
